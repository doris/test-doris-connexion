package test.doris.test.doris;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.BearerToken;
import com.google.api.client.auth.oauth2.ClientParametersAuthentication;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;

/**
 * Hello world!
 *
 */
public class App 
{
	
	
	private static final HttpTransport HTTP_TRANSPORT = new NetHttpTransport(); 

	/** Global instance of the JSON factory. */
	static final JsonFactory JSON_FACTORY = new GsonFactory();
	private static String API_KEY = "624c0b53e8c90dc2cf7aa5d6b4572c03";
	private static String API_SECRET = "d12e0548228dfe75de65848ff1e45411";
	public static String ACCESS_TOKEN = "e865c487a94532cd866272586b589bdb0104b7e9";
	
	public static final String DORIS_WEB_SERVER_HOST = "doris.ffessm.fr";

    public static final String TOKEN_SERVER_URL = "https://"+DORIS_WEB_SERVER_HOST+"/api/auth/oauth/token";
    public static final String AUTHORIZATION_SERVER_URL = "https://"+DORIS_WEB_SERVER_HOST+"/oauth/authorize";
    
    
    public static int testConnexion(String url, String accessToken) throws URISyntaxException, IOException, InterruptedException {
    	HttpClient client = HttpClient.newHttpClient();
    	
    	// Access token is sent both in header and in the URL, just in case
    	HttpRequest request = HttpRequest.newBuilder()
                .GET()
                //.header("Authorization", "Bearer "+accessToken)
                .uri(new URI(url + "?oauth_token="+accessToken))
                .build();
    
    	
    	HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
    	System.out.println(request.toString());
    	System.out.println(request.headers().toString());
    	System.out.println(response);
    	System.out.println("------");
    	return response.statusCode();
    }
    
    public static String testOAuthViaLocalBrowser(String userID) throws IOException {
    	
    	AuthorizationCodeFlow flow = new AuthorizationCodeFlow.Builder(BearerToken.authorizationHeaderAccessMethod(), HTTP_TRANSPORT,
				JSON_FACTORY, new GenericUrl(TOKEN_SERVER_URL), new ClientParametersAuthentication(
						API_KEY, API_SECRET),
				API_KEY, AUTHORIZATION_SERVER_URL)/* setScopes(Arrays.asList(SCOPE)).setDataStoreFactory(DATA_STORE_FACTORY)
																											 */.build();
		// authorize
    	// will ask to open a browser on the localhost so the user can enter his login and password
		LocalServerReceiver receiver = new LocalServerReceiver.Builder().setHost("localhost")
				.setPort(8087).build();

		
		Credential cred = new AuthorizationCodeInstalledApp(flow, receiver).authorize(userID);
		System.out.println( "AccessToken obtained: "+ cred.getAccessToken());
		return cred.getAccessToken();
    }
    
    // https://doris.ffessm.fr/content/edit/135836/6/fre-FR
    // https://doris.ffessm.fr/content/history/135836/6
    public static void testUserProfileEdit() throws URISyntaxException, IOException, InterruptedException {
    
    	HttpClient client = HttpClient.newHttpClient();
    	HttpRequest request = HttpRequest.newBuilder()
                .GET()
                .header("Authorization", "Bearer "+ACCESS_TOKEN)
                .uri(new URI("https://doris.ffessm.fr/api/ezp/v2/content/sections"))
                .build();
    
    	
    	HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
    	System.out.println(response);
    }
    
   
    
    public static void main( String[] args ) throws IOException, URISyntaxException, InterruptedException
    {
    	// Interactive: ACCESS_TOKEN creation
    	ACCESS_TOKEN = testOAuthViaLocalBrowser(args[0]);
    	
    	// use of the ACCESS TOKEN on the API (check the error code
    	testConnexion("https://doris.ffessm.fr/api/ezx/v1/object/171365",ACCESS_TOKEN);  // Groupe PROCARYOTES
    	testConnexion("https://doris.ffessm.fr/api/ezx/v1/object/48868",ACCESS_TOKEN);  // Groupe Procaryotes
    	
    	
    	// espèces
    	testConnexion("http://doris.ffessm.fr/api/ezp/v1/content/node/66/childrenCount", ACCESS_TOKEN);
    	testConnexion("http://doris.ffessm.fr/api/ezp/v1/content/node/66", ACCESS_TOKEN); 
    	testConnexion("http://doris.ffessm.fr/api/ezp/v1/content/node/66/list/limit/50", ACCESS_TOKEN); 
    	
    	//intervenants
    	testConnexion("https://doris.ffessm.fr/api/ezp/v1/content/node/77/childrenCount", ACCESS_TOKEN);
    	testConnexion("https://doris.ffessm.fr/api/ezp/v1/content/node/77/list/limit/50", ACCESS_TOKEN);
    	// glossaire
    	testConnexion("https://doris.ffessm.fr/api/ezp/v1/content/node/69/childrenCount", ACCESS_TOKEN);
    	
    	// Fiches
    	testConnexion("https://doris.ffessm.fr/api/ezp/v1/content/node/66/childrenCount", ACCESS_TOKEN);
    	
        System.out.println( "End" );
    }
}
