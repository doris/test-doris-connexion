package test.doris.test.doris;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     * @throws IOException 
     */
    @Test
    public void basicTest() throws IOException
    {
    	String accessToken = App.testOAuthViaLocalBrowser("dvojtise@gmail.com");
    	assertNotNull(accessToken); 
        assertFalse( accessToken.isEmpty() );
    }
}
